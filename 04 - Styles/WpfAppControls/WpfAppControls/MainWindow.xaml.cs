﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Collections.ObjectModel;

namespace WpfAppControls
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Image> Images { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            Images = new ObservableCollection<Image>
            {
                new Image { Photo = "space.jpg", Title = "Space" },
                new Image { Photo = "cat.jpg", Title = "Cat" },
                new Image { Photo = "cat.jpg", Title = "Cat" },
                new Image { Photo = "cat.jpg", Title = "Cat" }
            };

            imagesListBox.ItemsSource = Images;
        }

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    CommonOpenFileDialog dialog = new CommonOpenFileDialog();
        //    dialog.InitialDirectory = "C:\\Users";
        //    dialog.IsFolderPicker = true;
        //    if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
        //    {
        //        MessageBox.Show("You selected: " + dialog.FileName);
        //    }

        //    if (middleRow.Height.Value == 0)
        //        middleRow.Height = new GridLength(1, GridUnitType.Star);
        //    else
        //        middleRow.Height = new GridLength(0);
        //}

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    if (textBlock.Visibility == Visibility.Collapsed)   
        //        textBlock.Visibility = Visibility.Visible;
        //    else
        //        textBlock.Visibility = Visibility.Collapsed;
        //}
    }

    public class Image 
    {
        public string Photo { get; set; }
        public string Title { get; set; }
    }
}
