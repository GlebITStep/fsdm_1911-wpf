﻿using System;
using System.Collections.Generic;
using System.Text;
using ContactsAppWpf.Models;
using ContactsAppWpf.ViewModels;
using FluentValidation;

namespace ContactsAppWpf.Validators
{
    class ContactValidator : AbstractValidator<ContactEditorViewModel>
    {
        public ContactValidator()
        {
            RuleFor(x => x.FullName)
                .NotEmpty()
                .Length(2, 100);

            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress();

            RuleFor(x => x.Phone)
                .NotEmpty()
                .Matches("^[0-9,-]{2,20}$");
        }
    }
}
