﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using ContactsAppWpf.Validators;
using GalaSoft.MvvmLight;

namespace ContactsAppWpf.ViewModels
{
    class ContactEditorViewModel : ViewModelBase, IDataErrorInfo
    {
        private readonly ContactValidator _validator;

        public ContactEditorViewModel(ContactValidator validator)
        {
            _validator = validator;
        }

        private string _fullName;
        public string FullName
        {
            get => _fullName;
            set => Set(ref _fullName, value);
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => Set(ref _email, value);
        }

        private string _phone;
        public string Phone
        {
            get => _phone;
            set => Set(ref _phone, value);
        }

        private string _bio;
        public string Bio
        {
            get => _bio;
            set => Set(ref _bio, value);
        }

        public string Error { get; }

        public string this[string columnName]
        {
            get
            {
                var result = _validator.Validate(this);

                if (result.IsValid)
                    return string.Empty;

                var error = result.Errors
                    .FirstOrDefault(x => x.PropertyName == columnName);

                if (error is null)
                    return string.Empty;

                return error.ErrorMessage;
            }
        }
    }
}
