﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Windows.Data;
using ContactsAppWpf.Models;
using GalaSoft.MvvmLight;

namespace ContactsAppWpf.ViewModels
{
    class ContactListViewModel : ViewModelBase
    {
        private ObservableCollection<Contact> _contacts;
        public ObservableCollection<Contact> Contacts
        {
            get => _contacts;
            set => Set(ref _contacts, value);
        }

        private string _text = string.Empty;
        public string Text
        {
            get => _text;
            set
            {
                Set(ref _text, value);
                ContactsView.Refresh();
            }
        }

        public ICollectionView ContactsView { get; set; }

        public ContactListViewModel()
        {
            var json = File.ReadAllText("data.json");
            Contacts = JsonSerializer.Deserialize<ObservableCollection<Contact>>(json);
            ContactsView = CollectionViewSource.GetDefaultView(Contacts);
            ContactsView.Filter = FilterByName;
        }

        private bool FilterByName(object obj)
        {
            if (obj is Contact contact)
            {
                return contact.FullName.ToLower().StartsWith(Text.ToLower());
            }
            return false;
        }

    }
}
