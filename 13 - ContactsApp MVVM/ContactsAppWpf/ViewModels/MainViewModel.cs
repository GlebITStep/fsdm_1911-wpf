﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace ContactsAppWpf.ViewModels
{
    class MainViewModel : ViewModelBase
    {
        private ViewModelBase _currentViewModel;
        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set => Set(ref _currentViewModel, value);
        }

        public MainViewModel()
        {
            
        }

        private RelayCommand _goToDasboardCommand;
        public RelayCommand GoToDashboardCommand => _goToDasboardCommand ??= new RelayCommand(
            () => CurrentViewModel = App.Services.GetInstance<DashboardViewModel>());


        private RelayCommand _goToListCommand;
        public RelayCommand GoToListCommand => _goToListCommand ??= new RelayCommand(
            () => CurrentViewModel = App.Services.GetInstance<ContactListViewModel>());


        private RelayCommand _goToEditorCommand;
        public RelayCommand GoToEditorCommand => _goToEditorCommand ??= new RelayCommand(
            () => CurrentViewModel = App.Services.GetInstance<ContactEditorViewModel>());
    }
}
