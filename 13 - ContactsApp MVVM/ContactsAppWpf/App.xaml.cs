﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ContactsAppWpf.Validators;
using ContactsAppWpf.ViewModels;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using SimpleInjector;

namespace ContactsAppWpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Container Services;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            RegisterServices();
            Start<DashboardViewModel>();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            //MessageBox.Show("Bye!");
            base.OnExit(e);
        }

        private void RegisterServices()
        {
            Services = new Container();

            Services.RegisterSingleton<IMessenger, Messenger>();
            Services.RegisterSingleton<DashboardViewModel>();
            Services.RegisterSingleton<ContactValidator>();
            Services.RegisterSingleton<ContactListViewModel>();
            Services.RegisterSingleton<ContactEditorViewModel>();
            Services.RegisterSingleton<MainViewModel>();

            Services.Verify();
        }

        private void Start<T>() where T : ViewModelBase
        {
            var windowViewModel = Services.GetInstance<MainViewModel>();
            windowViewModel.CurrentViewModel = Services.GetInstance<T>();
            var window = new MainWindow { DataContext = windowViewModel };
            window.ShowDialog();
        }
    }
}
