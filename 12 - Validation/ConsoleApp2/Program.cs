﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using FluentValidation;

namespace ConsoleApp2
{
    //class AuthorAttribute : Attribute
    //{
    //    public string Name { get; set; }

    //    public AuthorAttribute(string name)
    //    {
    //        Name = name;
    //    }
    //}

    //class PassportNumberAttribute : ValidationAttribute
    //{
    //    public string Prefix { get; set; }

    //    public override bool IsValid(object value)
    //    {
    //        if (value is string passportNumber)
    //        {
    //            var regex = new Regex("^[A-Z]{1,3}[0-9]{6,10}$");
    //            var isRegexMatch = regex.IsMatch(passportNumber);
    //            if (isRegexMatch)
    //            {
    //                if (!string.IsNullOrWhiteSpace(Prefix))
    //                {
    //                    return passportNumber.StartsWith(Prefix);
    //                }
    //                return true;
    //            }
    //        }
    //        return false;
    //    }
    //}

    ////[Author("Gleb")]
    //class Person
    //{
    //    [Required]
    //    [MinLength(2, ErrorMessage = "Name is too short!")]
    //    [MaxLength(20)]
    //    public string Name { get; set; }

    //    [MinLength(2)]
    //    [MaxLength(20)]
    //    public string Surname { get; set; }

    //    [Required]
    //    [EmailAddress]
    //    public string Email { get; set; }

    //    [Required]
    //    [Range(0, 100)]
    //    public int Age { get; set; }

    //    [Phone]
    //    public string Phone { get; set; }

    //    [CreditCard]
    //    public string CardNumber { get; set; }

    //    //[RegularExpression("^[A-Z]{1,3}[0-9]{6,10}$")]
    //    //[PassportNumber]
    //    [PassportNumber(Prefix = "AZE")]
    //    public string PassportNumber { get; set; }
    //}

    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        var person = new Person
    //        {
    //            Name = "Gleb",
    //            Email = "skripnikov@itstep.org",
    //            Age = 26,
    //            PassportNumber = "AZE123456"
    //        };

    //        var context = new ValidationContext(person);
    //        List<ValidationResult> results = new List<ValidationResult>();
    //        var isValid = Validator.TryValidateObject(person, context, results, true);

    //        Console.WriteLine($"Is valid: {isValid}");
    //        if (!isValid)
    //        {
    //            foreach (var validationResult in results)
    //            {
    //                Console.WriteLine(validationResult.ErrorMessage);
    //            }
    //        }
    //        Console.WriteLine();

    //        ////REFLECTION
    //        //var personType = typeof(Person);
    //        //foreach (var propertyInfo in personType.GetProperties())
    //        //{
    //        //    Console.WriteLine(propertyInfo.Name);
    //        //    foreach (var attribute in propertyInfo.CustomAttributes)
    //        //    {
    //        //        Console.WriteLine(attribute);
    //        //    }
    //        //    Console.WriteLine();
    //        //}
    //    }
    //}

    class PersonValidator : AbstractValidator<Person>
    {
        public PersonValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MinimumLength(2)
                .MaximumLength(20);
                //.WithMessage("Something wrong!");

            RuleFor(x => x.PassportNumber)
                .Must(ValidatePassportNumber)
                .WithMessage("Invalid passport!");
        }

        private bool ValidatePassportNumber(string passportNumber)
        {
            var regex = new Regex("^[A-Z]{1,3}[0-9]{6,10}$");
            var isRegexMatch = regex.IsMatch(passportNumber);
            if (isRegexMatch)
            {
                if (!string.IsNullOrWhiteSpace("AZE"))
                {
                    return passportNumber.StartsWith("AZE");
                }
                return true;
            }
            return false;
        }
    }

    class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
        public string CardNumber { get; set; }
        public string PassportNumber { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person
            {
                Name = "G",
                Email = "skripnikov@itstep.org",
                Age = 26,
                PassportNumber = "AZ123456"
            };

            var personValidator = new PersonValidator();
            var result = personValidator.Validate(person);

            Console.WriteLine($"Is valid: {result.IsValid}");
            foreach (var error in result.Errors)
            {
                Console.WriteLine(error.ErrorMessage);
            }
        }
    }
}
