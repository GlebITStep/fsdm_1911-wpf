﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppTemplates
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Uri uri = new Uri(@"C:\Users\XPS\Desktop\WPFCalc.png", UriKind.Absolute);
            ImageSource imgSource = new BitmapImage(uri);
            myImage.Source = imgSource;

            FileInfo fileInfo = new FileInfo(@"C:\Users\XPS\Desktop\WPFCalc.png");
        }
    }
}
