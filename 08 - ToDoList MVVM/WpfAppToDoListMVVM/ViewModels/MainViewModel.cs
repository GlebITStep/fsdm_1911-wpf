﻿using MVVMTools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using WpfAppToDoListMVVM.Models;

namespace WpfAppToDoListMVVM.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private string taskName;
        private string taskDescription;
        private ObservableCollection<MyTask> myTasks;
        private bool taskDone;
        private DateTime taskDeadline = DateTime.Now;

        public MainViewModel()
        {
            MyTasks = new ObservableCollection<MyTask>();
        }



        public string TaskName 
        { 
            get => taskName;
            set 
            {
                OnChanged(out taskName, value);
                AddTaskCommand.RaiseCanExecuteChanged();
            }  
        }


        public string TaskDescription { get => taskDescription; set => OnChanged(out taskDescription, value); }
        public bool TaskDone { get => taskDone; set => OnChanged(out taskDone, value); }
        public DateTime TaskDeadline { get => taskDeadline; set => OnChanged(out taskDeadline, value); }
        public ObservableCollection<MyTask> MyTasks { get => myTasks; set => OnChanged(out myTasks, value); }


        private Command addTaskCommand;
        public Command AddTaskCommand => addTaskCommand ?? (addTaskCommand = new Command(
            () =>
            {
                MyTasks.Add(new MyTask
                {
                    Name = TaskName,
                    Description = TaskDescription,
                    Deadline = TaskDeadline,
                    IsDone = TaskDone
                });

                Clear();
            }, 
            () => !string.IsNullOrWhiteSpace(TaskName)));



        //private Command removeTaskCommand = null;
        //public Command RemoveTaskCommand => removeTaskCommand ?? (removeTaskCommand = new Command(
        //    () =>
        //    {
        //        if (SelectedTask != null)
        //        {
        //            MyTasks.Remove(SelectedTask);
        //        }
        //    }));


        private Command<MyTask> removeTaskCommand = null;
        public Command<MyTask> RemoveTaskCommand => removeTaskCommand ?? (removeTaskCommand = new Command<MyTask>(
            task =>
            {
                MyTasks.Remove(task);
            }));


        private Command<string> buttonClickCommand = null;
        public Command<string> ButtonClickCommand => buttonClickCommand ?? (buttonClickCommand = new Command<string>(
            param =>
            {
                MessageBox.Show(param.ToUpper());
            }));

        public void Clear()
        {
            TaskName = string.Empty;
            TaskDescription = string.Empty;
            TaskDeadline = DateTime.Now;
            TaskDone = false;
        }
    }
}
