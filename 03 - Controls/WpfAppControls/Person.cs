﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfAppControls
{
    public class Person
    {
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}
